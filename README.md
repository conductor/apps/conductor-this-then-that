# This Then That App
App that is used for listening on events from Conductor and react to them, similar to how IFTTT works.

# Installation Instructions
- Change the url and api key in imports/configuration/config.js.
- Double click on start.bat
- Open http://localhost:4000 in your webbrowser.