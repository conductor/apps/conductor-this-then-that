import { log } from '../logger/logger.js'

Devices.find({}).observe({
    changed: function(device, oldDevice) {
        var componentMap = createComponentMap(device.components);
        var properties = PropertyDiff.diff(device.components, oldDevice.components);

        if (properties.length == 0) {
            log.info('No properties changed.');
        }

        _.each(properties, function(property) {
            property.componentId = componentMap[property.id];
            log.info('Property "%s" (%s) in component "%s" was changed to %s.', property.name, property.id, property.componentId, property.value);

            if (property.componentId == null) {
                log.error('Component map doesn\'t contain the property id {}', property.id);
            }

            var workspaces = Workspaces.find({
                'variables.deviceId': device.id,
                'variables.componentId': property.componentId,
                'variables.propertyId': property.id
            }).fetch();

            log.info('%s workspaces had a variable that matched the property that was changed.', workspaces.length);

            _.each(workspaces, function(workspace) {
                log.info('Updating and evaluating workspace "%s" (%s).', workspace.name, workspace._id);
                workspace.variables = updateVariables(workspace, property);
                log.info('Variables after updating property values.', workspace.variables);
                saveVariables(workspace);

                if (workspace.enabled) {
                    var evaluator = makeEvalContext(workspace.variables);
                    evaluator(workspace.code);
                    var actions = evaluator('updatedVariables;');

                    _.each(actions, function(action) {
                        log.info('Sending action to conductor: ', action)
                        var filter = {
                            componentId: action.componentId,
                            propertyId: action.propertyId
                        };

                        Conductor.setPropertyValue(filter, action.value, function(error, result) {
                            if (error) {
                                log.error(error);
                            } else {
                                log.info(result);
                            }
                        });
                    })
                } else {
                    log.info('Workspace is disabled, skipping.');
                }
            })
        })
    }
});

function makeEvalContext(variables) {
    var updatedVariables = [];

    function variableValue(variableName) {
        return variableRef(variableName).value;
    }

    function variableRef(variableName) {
        return findVariable(variableName, variables);
    }

    function findVariable(variableName, variables) {
        var variableParts = variableName.split('/');
        var foundVariable = _.findWhere(variables, {
            deviceId: variableParts[0],
            componentId: variableParts[1],
            propertyId: variableParts[2]
        });

        if (foundVariable) {
            return foundVariable;
        } else {
            throw new Error('Couldn\'t find variable ' + variableName + '.');
        }
    }

    function setVariable(variableRef, value) {
        variableRef.value = value;
        updatedVariables.push(variableRef);
    }

    function hasChanged(variableRef) {
        return variableRef.hasChanged;
    }

    return function(evalStr) {
        return eval(evalStr);
    }
}

function createComponentMap(components) {
    var map = {};

    _.each(components, function(component) {
        _.each(component.properties, function(property) {
            map[property.id] = component.id;
        })
    })

    return map;
}

 function updateVariables(workspace, property) {
    return _.map(workspace.variables, function(variable) {
        if (variable.componentId === property.componentId && variable.propertyId === property.id) {
            variable.value = property.value;
            variable.hasChanged = true;
        }

        return variable;
    });
 }

function saveVariables(workspace) {
    var workspaceCopy = JSON.parse(JSON.stringify(workspace));

    workspaceCopy.variables = _.map(workspaceCopy.variables, function(variable) {
        variable.hasChanged = false;
        return variable;
    })

    Workspaces.update(workspaceCopy.id, workspaceCopy);
}
