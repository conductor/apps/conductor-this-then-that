import '../collections/collections.js';
import { log } from '../logger/logger.js'

 Meteor.methods({
     'clearDatabase' () {
         Meteor.users.remove({});
         Devices.remove({});
         Triggers.remove({});
    },
    'saveWorkspace' (workspaceId, name, xml, code) {
        log.info('Saving workspace "%s".', name, xml);
        var variables = findVariables(code);
        var record = {
            name: name,
            xml: xml,
            code: code,
            variables: variables,
            enabled: true
        };

        if (workspaceId) {
            Workspaces.update(workspaceId, record);
        } else {
            Workspaces.insert(record);
        }

        return xml;
    },
    'removeWorkspace' (workspaceId) {
        Workspaces.remove(workspaceId);
    },
    'changeWorkspaceStatus' (workspaceId, enabled) {
        var workspace = Workspaces.findOne(workspaceId);
        workspace.enabled = enabled;
        Workspaces.update(workspaceId, workspace);
    }
});

function findVariables(code) {
    var variables = [];
    variables = variables.concat(findVariablesByFunctionName(code, 'variableRef'));
    variables = variables.concat(findVariablesByFunctionName(code, 'variableValue'));
    return convertVariables(variables);
}

function findVariablesByFunctionName(code, functionName) {
    var start = 0;
    var variables = [];
    var functionName = functionName + '("';

    while (start != -1) {
        start = code.indexOf(functionName, start);

        if (start != -1) {
            start = start + functionName.length;
            var end = code.indexOf('"', start);
            var variable = code.substring(start, end);
            variables.push(variable);
        }
    }

    return variables;
}

function convertVariables(variables) {
    return _.map(variables, function(variable) {
        return convertVariable(variable);
    });
}

function convertVariable(variable) {
    var parts = variable.split('/');

    return {
        deviceId: parts[0],
        componentId: parts[1],
        propertyId: parts[2],
        value: null,
        hasChanged: false
    }
}
