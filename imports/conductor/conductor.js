import { log } from '../logger/logger.js'

Conductor = require('node-conductor');
Conductor.connect(Meteor.settings.conductor.url);
Conductor.setApiKey(Meteor.settings.conductor.apiKey);
Conductor.subscribe(Conductor.Subscription.DEVICES);
Conductor.subscribe(Conductor.Subscription.SET_PROPERTY_VALUE_REQUEST_SUMMARIES);
Conductor.subscribe(Conductor.Subscription.APPLICATION_INVITES);

Conductor.Devices.find({}).observe({
    added: function (device) {
        log.info('A device was added.', device);

        var existingDevice = Devices.findOne({ id: device.id });
        if (existingDevice) {
            Devices.update({ id: device.id }, device);
        } else {
            Devices.insert(device);
        }
    },
    changed: function(device, oldDevice) {
        //log.debug('A device was changed.', device);

        var existingDevice = Devices.findOne({ id: device.id });
        if (existingDevice) {
            Devices.update({ id: device.id }, device);
        } else {
            Devices.insert(device);
        }
    },
    removed: function (device) {
        log.info("A device was removed.", device);

        var existingDevice = Devices.findOne({ id: device.id });
        if (existingDevice) {
            Devices.remove({ id: device.id });
        }
    }
});

