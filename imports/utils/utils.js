Utility = {

    containsKey: function(obj, key) {
        return obj.hasOwnProperty(key);
    },

    isFunction: function(key) {
        return Utility.isString(key) && key.startsWith('#');
    },

    isVariable: function(key) {
        return Utility.isString(key) && key.startsWith('@variable');
    },

    isObject: function(value) {
        return value !== null && typeof value === 'object' && !Utility.isArray(value);
    },

    isArray: function(value) {
        return Object.prototype.toString.call(value) === '[object Array]';
    },

    isBoolean: function(value) {
        return typeof value === 'boolean';
    },

    isString: function(value) {
        return typeof value === 'string';
    }

}