PropertyDiff = {

    diff: function(newComponents, oldComponents) {
        var diff = [];
        var newPropertyMap = PropertyDiff.createPropertyMap(newComponents);
        var oldPropertyMap = PropertyDiff.createPropertyMap(oldComponents);

        _.each(newPropertyMap, function(newProperty, key) {
            var oldProperty = oldPropertyMap[key];

            if (oldProperty == null || oldProperty.value !== newProperty.value) {
                diff.push(newProperty);
            }
        });

        return diff;
    },

    createPropertyMap: function(components) {
        var propertyMap = {};

        _.each(components, function(component) {
            _.each(component.properties, function(property) {
                propertyMap[property.id] = property;
            });
        });

        return propertyMap;
    }
}