import winston from 'winston'
import winstonLogstash from 'winston-logstash'
import winstonLogzio from 'winston-logzio';

const log = winston;

winston.remove(winston.transports.Console);

if (Meteor.settings.logger.console.enable) {
    winston.add(winston.transports.Console, Meteor.settings.logger.console.options);
    log.info('Logging to console enabled.');
}

if (Meteor.settings.logger.file.enable) {
    winston.add(winston.transports.File, Meteor.settings.logger.file.options);
    log.info('Logging to file enabled.');
}

if (Meteor.settings.logger.logstash.enable) {
    winston.add(winston.transports.Logstash, Meteor.settings.logger.logstash.options);
    log.info('Logging to logstash enabled.');
}

if (Meteor.settings.logger.logzio.enable) {
    winston.add(winstonLogzio, Meteor.settings.logger.logzio.options);
    log.info('Logging to logzio enabled.');
}

export { log }