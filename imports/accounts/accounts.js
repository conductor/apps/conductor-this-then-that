import { log } from '../logger/logger.js'

Accounts.onCreateUser(function (options, user) {
    if (options.profile) {
         user.profile = options.profile;
    }

    log.info('User was created.', user);

    Conductor.createInvite(user._id, function(error, result) {
        if (error) {
            log.error('An error occured when sending "createInvite" request to conductor.', error);
        } else {
            log.info('Successfully created a new invite.', result);
            Meteor.users.update({ _id: user._id }, {'$set' : {'profile.inviteUrl' : result.inviteUrl }});
        }
    });

    return user;
})

Conductor.Invites.find({}).observe({
    changed: function(invite, oldInvite) {
        log.info('Invite updated.', invite);

        if (invite.userPublicKey) {
            Meteor.users.update({ _id: invite.externalId }, { '$set': { 'profile.publicKey': invite.userPublicKey }});
        }

        if (invite.applicationInviteStatus === 'COMPLETED') {
            Meteor.users.update({ _id: invite.externalId }, { '$set': { 'profile.inviteStatus': 'COMPLETED' }});
        }
    }
});

ServiceConfiguration.configurations.upsert(
  { service: 'google' },
  {
    $set: {
      clientId: Meteor.settings.authentication.google.clientId,
      secret: Meteor.settings.authentication.google.clientSecret
    }
  }
);