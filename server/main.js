import { Meteor } from 'meteor/meteor';
import { log } from '../imports/logger/logger.js'
import '../imports/utils/utils.js';
import '../imports/utils/propertyDiff.js';
import '../imports/collections/collections.js';
import '../imports/conductor/conductor.js';
import '../imports/accounts/accounts.js';
import '../imports/publications/publications.js';
import '../imports/trigger/trigger.js';
import '../imports/methods/methods.js';

Meteor.startup(() => {
    log.info("Starting application.");
});
