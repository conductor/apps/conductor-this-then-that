import {
    Meteor
} from 'meteor/meteor';
import {
    Template
} from 'meteor/templating';

import './requireAuthentication.html';

Template.requireAuthentication.helpers({
    user: function() {
        return Meteor.user();
    },
    inviteAccepted: function() {
        return Meteor.user().profile.publicKey;
    }
});

function redirectToLogin() {
    console.log('Redirecting to login screen.');

    // Redirect to login page, and pass on the redirect url, so we can go back to this page when the login is completed.
    var queryParams = FlowRouter.current().queryParams;
    queryParams['redirectUrl'] = FlowRouter.current().path;
    var url = FlowRouter.path('login', FlowRouter.current().params, queryParams);
    FlowRouter.go(url);
}

function redirectToInvitePage() {
    console.log('Redirecting to invite screen.');

     // Redirect to invite page, and pass on the redirect url, so we can go back to this page when the invite process is completed.
    var queryParams = FlowRouter.current().queryParams;
    queryParams['redirectUrl'] = FlowRouter.current().path;
    var url = FlowRouter.path('invite', FlowRouter.current().params, queryParams);
    FlowRouter.go(url);
}
