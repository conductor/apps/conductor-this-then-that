import {
    Meteor
} from 'meteor/meteor';
import {
    Template
} from 'meteor/templating';

import './app.html';

Template.mainLayout.helpers({
    user: function() {
        return Meteor.user();
    }
});

Template.mainLayout.events({
    'click .logout' () {
        Meteor.logout();
        FlowRouter.go('home');
    }
});