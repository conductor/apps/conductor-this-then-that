import {
    Meteor
} from 'meteor/meteor';
import {
    Template
} from 'meteor/templating';
import {
    HTTP
} from 'meteor/http'

import './login.html';

Template.login.events({
    'click .do-login' (event) {
       Meteor.loginWithGoogle({
          requestPermissions: ['email']
        }, (err) => {
          if (err) {
            console.error('Failed to login.', err);
          } else {
            console.log('Succuessfully logged in.');
          }
        });
    }
});

