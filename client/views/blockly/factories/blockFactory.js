BlockFactory = {
    createDefaultBlocks: function() {
        BlockFactory.createSetPropertyBlock();
        BlockFactory.createHasChangedBlock();
    },
    createSetPropertyBlock: function() {
        Blockly.Blocks['set_property'] = {
            init: function() {
                this.appendValueInput('SET').appendField("set");
                this.appendValueInput('TO').appendField("to");

                this.setInputsInline(true);
                this.setColour(200);
                this.setNextStatement(true);
                this.setPreviousStatement(true);
                console.log(this);
            }
        };

        Blockly.JavaScript['set_property'] = function(block) {
            set = Blockly.JavaScript.valueToCode(block, 'SET', Blockly.JavaScript.ORDER_NONE) || 'false';
            to = Blockly.JavaScript.valueToCode(block, 'TO', Blockly.JavaScript.ORDER_NONE) || 'false';
            set = set.replace('variableValue', 'variableRef');
            return 'setVariable(' + set + ', ' + to + ');\n';
        };
    },
    createHasChangedBlock: function() {
        Blockly.Blocks['has_changed'] = {
            init: function() {
                this.appendValueInput('VARIABLE').appendField("has changed");

                this.setInputsInline(true);
                this.setColour(200);
                this.setOutput(true, 'Boolean');
            }
        };

        Blockly.JavaScript['has_changed'] = function(block) {
            var variable = Blockly.JavaScript.valueToCode(block, 'VARIABLE', Blockly.JavaScript.ORDER_NONE) || null;
            variable = variable.replace('variableValue', 'variableRef');
            var code = 'hasChanged(' + variable + ')';
            return [code, Blockly.JavaScript.ORDER_MEMBER];
        };
    },
    createDeviceBlock: function(device) {
        Blockly.Blocks['device-' + device.id] = {
            init: function() {
                this.appendValueInput(device.id)
                    .setCheck('device-' + device.id)
                    .appendField(device.name);
                this.setOutput(true, 'Number');
                this.setColour(260);
                this.data = device.id;
            }
        };

        Blockly.JavaScript['device-' + device.id] = function(block) {
            var deviceId = block.inputList[0].name;
            var childCode = Blockly.JavaScript.valueToCode(block, device.id, Blockly.JavaScript.ORDER_ADDITION);
            var code = 'variableValue("' + deviceId + '/' + childCode + '")';

            // Set the output of this block to the output of the last child.
            var lastChildBlock = getLastChildBlock(block);
            var output = lastChildBlock.outputConnection.check_[1];
            block.outputConnection.check_ = [output];

            return [code, Blockly.JavaScript.ORDER_MEMBER];
        };
    },
    createComponentBlock: function(device, component) {
        Blockly.Blocks['component-' + component.id] = {
            init: function() {
                this.appendValueInput(component.id)
                    .setCheck('component-' + component.id)
                    .appendField(component.name);
                this.setOutput(true, 'device-' + device.id);
                this.setColour(300);
            }
        };

        Blockly.JavaScript['component-' + component.id] = function(block) {
            var componentId = block.inputList[0].name;
            var childCode = Blockly.JavaScript.valueToCode(block, component.id, Blockly.JavaScript.ORDER_ADDITION);
            var code = componentId + '/' + childCode;
            return [code, Blockly.JavaScript.ORDER_MEMBER];
        };
    },
    createPropertyBlock: function(component, property) {
        Blockly.Blocks['property-' + property.id] = {
          init: function() {
            this.appendDummyInput(property.id)
              .appendField(property.name);
            this.setOutput(true, 'component-' + component.id);
            this.setColour(330);
            var dataType = convertDataType(property.dataType);
            console.log(property);
            console.log(dataType);
            this.outputConnection.check_.push(dataType);
          }
        };

        Blockly.JavaScript['property-' + property.id] = function(block) {
          var propertyId = block.inputList[0].name;
          var code = propertyId;
          return [code, Blockly.JavaScript.ORDER_MEMBER];
        };
      }
}

function convertDataType(dataType) {
    if (dataType === 'boolean') {
        return 'Boolean';
    } else if (dataType === 'integer' || dataType ==='double') {
        return 'Number';
    } else if (dataType === 'string') {
        return 'String';
    }
}

/**
 * Get the last child block.
 * Assumes that all child blocks only have one input block.
 */
function getLastChildBlock(block) {
    if (block.childBlocks_.length == 0) {
        return block;
    } else {
        return getLastChildBlock(block.childBlocks_[0]);
    }
}