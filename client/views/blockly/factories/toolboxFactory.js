ToolboxFactory = {
    createToolbox: function(devices) {
        var toolbox = ' \
        <xml id="toolbox" style="display: none"> \
        <category name="Control"> \
            <block type="controls_if"></block> \
            <block type="controls_whileUntil"></block> \
        </category> \
        <category name="Logic"> \
            <block type="logic_compare"></block> \
            <block type="logic_operation"></block> \
            <block type="logic_boolean"></block> \
            <block type="logic_negate"></block> \
            <block type="logic_ternary"></block> \
        </category> \
        <category name="Math"> \
            <block type="math_arithmetic"></block> \
       </category> \
        <category name="Numbers"> \
            <block type="math_number"> \
                <field name="NUM">1</field> \
            </block> \
       </category> \
        <category name="Text"> \
            <block type="text"> \
                <field name="TEXT">Text</field> \
            </block> \
       </category> \
        <category name="Conductor" colour="150"> \
            <block type="set_property"></block> \
            <block type="has_changed"></block>';

        _.each(devices, function(device) {
            toolbox += '<category name="' + device.name + '" colour="260">';
            console.log(device.id);
            toolbox += '<block type="device-' + device.id + '"></block>';

            _.each(device.components, function(component) {
                toolbox += '<category name="' + component.name + '" colour="300">';
                toolbox += '<block type="component-' + component.id + '"></block>';
                toolbox += '<category name="properties" colour="330">';

                _.each(component.properties, function(property) {
                    toolbox += '<block type="property-' + property.id + '"></block>';
                })

                toolbox += '</category>';
                toolbox += '</category>';
            })

            toolbox += '</category>';
        })

        toolbox += '  </category>';
        toolbox += '  </xml>';

        return toolbox;
    }
}