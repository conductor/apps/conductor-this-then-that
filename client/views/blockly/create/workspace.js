import {
    Meteor
} from 'meteor/meteor';
import {
    Template
} from 'meteor/templating';
import {
    HTTP
} from 'meteor/http'

import Collections from '/client/collections/collections.js'

import './workspace.html';

var template = Template.createWorkspace;

template.onCreated(function() {
    this.subscribe('devices');
    this.subscribe('workspaces');
    template.workspaceName = new ReactiveVar('Untitled Workspace');
});

template.onRendered(function() {
    this.autorun(function() {
        if (this.subscriptionsReady()) {
            BlockFactory.createDefaultBlocks();
            updateToolbox();

            var workspaceId = FlowRouter.getParam('id');

            if (workspaceId != null) {
                restoreWorkspace(workspaceId);
            }
        }
    }.bind(this));
});

template.onDestroyed(function () {
       // Remove the toolbox when the user goes to a different page, there is a bug which causes the toolbox to be visible when switching page.
       deleteToolbox();
});

template.helpers({
    workspaceName: function() {
        return template.workspaceName.get();
    }
})

template.events({
    'click .save' (event) {
        console.log('Saving workspace: ' + template.workspaceName.get());
        var workspaceId = FlowRouter.getParam('id');
        var name = template.workspaceName.get();
        var xml = Blockly.Xml.workspaceToDom(workspace);
        var xmlText = Blockly.Xml.domToText(xml);
        var code = Blockly.JavaScript.workspaceToCode(workspace);

        Meteor.call('saveWorkspace', workspaceId, name, xmlText, code, function(error, result) {
            if (error) {
                alert('Failed to save workspace: ' + error);
            } else {
                console.log('Successfully saved workspace: ', result);
                FlowRouter.go('/blockly/list');
            }
        })
    },
    'click .go-back' (event) {
        console.log('go-back');
        FlowRouter.go('/blockly/list');
    },
    'change .workspace-name'(e) {
        template.workspaceName.set(e.target.value);
    }
});

var workspace;

function deleteToolbox() {
    $('.blocklyToolboxDiv').remove();
}

function updateToolbox() {
    $('#workspace').children().remove();
    deleteToolbox();

    var devices = Devices.find({}, { reactive: false }).fetch({});

    createBlocks(devices);

    var toolbox = ToolboxFactory.createToolbox(devices);

    workspace = Blockly.inject('workspace', {
        toolbox: toolbox
    });

    workspace.addChangeListener(debugOutput);
}

function createBlocks(devices) {
    _.each(devices, function(device) {
        BlockFactory.createDeviceBlock(device);
        _.each(device.components, function(component) {
            BlockFactory.createComponentBlock(device, component);
            _.each(component.properties, function(property) {
                BlockFactory.createPropertyBlock(component, property);
            });
        });
    })
}

function restoreWorkspace(workspaceId) {
    var dbWorkspace = Workspaces.findOne(workspaceId);
    template.workspaceName.set(dbWorkspace.name);
    var xml = Blockly.Xml.textToDom(dbWorkspace.xml);
    Blockly.Xml.domToWorkspace(workspace, xml);
}

function debugOutput() {
    var code = Blockly.JavaScript.workspaceToCode(workspace);
    document.getElementById('textarea').value = code;
}
