import {
    Meteor
} from 'meteor/meteor';
import {
    Template
} from 'meteor/templating';
import {
    HTTP
} from 'meteor/http'

import Collections from '/client/collections/collections.js'

import './listWorkspaces.html';

var template = Template.listWorkspaces;

template.onCreated(function() {
    this.subscribe('workspaces');
});

template.helpers({
    workspaces: function() {
        return Workspaces.find({});
    },
    isChecked: function() {
        return this.enabled ? 'checked' : '';
    }
});

template.events({
    'click .create' (event) {
        FlowRouter.go('/blockly/create');
    },
    'click .edit' (event) {
        FlowRouter.go('/blockly/edit/' + this._id);
    },
    'click .remove' (event) {
        console.log('Removing workspace: ', this);

        Meteor.call('removeWorkspace', this._id, function(error, result) {
            if (error) {
                console.error('Failed to remove workspace: ', error);
            } else {
                console.log('Response when removing workspace: ', result);
            }
        });
    },
    'click .enabled' (event) {
        var enabled = !this.enabled;
        console.log('Changing workspace enabled to: ' + enabled);
        Meteor.call('changeWorkspaceStatus', this._id, enabled, function(error, result) {
            if (error) {
                alert('Failed to change workspace status: ', error);
            }
        })
    }
});