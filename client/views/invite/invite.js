import {
    Meteor
} from 'meteor/meteor';
import {
    Template
} from 'meteor/templating';
import {
    HTTP
} from 'meteor/http'

import Collections from '/client/collections/collections.js'
import './invite.html';

var template = Template.invite;

template.events({
    'click .accept-invite' (event) {
        console.debug('Accep invite:', Meteor.user().profile.inviteUrl);
        template.inviteWindow = window.open(Meteor.user().profile.inviteUrl, '_blank', 'width=' + popupWindow.width() + ',height=' + popupWindow.height() + ',left=' + popupWindow.left() + ',top=' + popupWindow.top());
    }
});

var popupWindow = {
    left: function() {
        return window.outerWidth / 2 - popupWindow.width() / 2;
    },
    top: function() {
        return (window.outerHeight / 2 - popupWindow.height() / 2) * 0.33;
    },
    width: function() {
        return 700;
    },
    height: function() {
        return 750;
    }
}

Meteor.users.find().observe({
    changed: function(user) {
        if (user && user.profile && user.profile.inviteStatus === 'COMPLETED') {
            if (template.inviteWindow) {
                template.inviteWindow.close();
            }
        }
    }
})
