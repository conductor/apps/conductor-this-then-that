FlowRouter.route('/', {
  name: 'home',
  action: function() {
    BlazeLayout.render("mainLayout", {content: "home"});
  }
});

FlowRouter.route('/login', {
    name: 'login',
    action: function(params) {
      BlazeLayout.render( 'mainLayout', { content: 'login' } );
    }
});

FlowRouter.route('/invite', {
    name: 'invite',
    action: function(params) {
      BlazeLayout.render( 'mainLayout', { content: 'invite' } );
    }
});

FlowRouter.route('/blockly/list', {
    name: 'listWorkspaces',
    action: function(params) {
      BlazeLayout.render( 'mainLayout', { content: 'listWorkspaces' } );
    }
});

FlowRouter.route('/blockly/create', {
    name: 'createWorkspace',
    action: function(params) {
      BlazeLayout.render( 'mainLayout', { content: 'createWorkspace' } );
    }
});

FlowRouter.route('/blockly/edit/:id', {
    name: 'createWorkspace',
    action: function(params) {
      BlazeLayout.render( 'mainLayout', { content: 'createWorkspace' } );
    }
});
